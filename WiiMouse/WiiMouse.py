#!/usr/bin/env python
'''
' #!/usr/bin/env python
' #!c:/Python27/python.exe
'
' WiiMouse.py
'
' 	Wii remote control button/movement listener, combined with a Simple socket 
'   client using threads. Note that exact positioning depends on the Infra-Red 
'   (IR) Receiver of the Wii which we don't have, so stuck to just the 
'   accelerometer (X/Y), and button pushes for remote controlling stuff.
'''
import socket
import sys
import cwiid
import time

#connecting to the wiimote. This allows several attempts as first few often fail.
print('Press 1+2 on your Wiimote now...')
wii = None
i=2
while not wii:
	try:
		wii = cwiid.Wiimote()
	except RuntimeError:
		if (i>5):
			print("cannot create connection")			
			quit()
		print("Error opening wiimote connection")
		print("attempt " + str(i))
		i += 1

#set wiimote to report button presses and accelerometer state
wii.rpt_mode = cwiid.RPT_BTN | cwiid.RPT_ACC

#turn on led to show connected
wii.led = 1

###############################################################################
#begin setting up a connection, since we are paired with the Wiimote
HOST = sys.argv[1] or 'localhost' # default to Symbolic name meaning all available interfaces
PORT = int(sys.argv[2]) or 8888   # Arbitrary non-privileged port

MAX_MSG_SIZE = 20  # maximum simultaneous streaming/incoming characters until the$
running = True     # Helps us loop through waiting for input
button_delay = 0.2 #ms delay between button presses to prevent mashing or signal overload

screenW = 1280  #upper limit for Screen Width (set by handshake, with a default value just in case)
screenH = 720   #upper limit for Screen Height (set by handshake, with a default value just in case)
coords = ''     #coordinates as x,y positional string
x = 500			#default X to near center of average screen
y = 500			#default Y to near center of average screen


# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Connect the socket to the port where the server is listening
server_address = (HOST, PORT)
print('connecting to %s port %s' % server_address)
sock.connect(server_address)

#function for sending
def send(msg):
	print('Sending: %s' % msg)
	# Send data    
	sock.sendall(msg+"\n")
	
	# Look for the response
	amount_received = 0
	amount_expected = len(msg)

	# loop through response data
	while amount_received < amount_expected:
		data = sock.recv(MAX_MSG_SIZE)
		amount_received += len(data)
		##receive feedback for screen-specific sizing and keep track of position
		if ('|' in data):
			handshake = data.split('|')
			resolution = handshake[0]
			coordinates = handshake[1]			
			res = resolution.split('x')
			screenW = res[0]
			screenH = res[1]
			coords = coordinates.split(',')
			x = coords[0]
			y = coords[1]
		#only coordinates are actually coming back
		elif (',' in data):
			coords = data.split(',')
			x = coords[0]
			y = coords[1]		
		print('received: %s' % data)			
###############################################################################


#Wiimote accelerometer handling code
try:
	#print state every second
	while True:
		buttons = wii.state['buttons']
		
		# when Plus & Minus buttons pressed together then rumble and quit.
		if (buttons - cwiid.BTN_PLUS - cwiid.BTN_MINUS == 0):  
			print '\nClosing connection ...'
			wii.rumble = 1
			time.sleep(1)
			send("BYE")
			wii.rumble = 0
			exit(wii)
			
		# when 1 & 2 buttons pressed together and accelerometer changes, scroll the page in direction of change
		elif (buttons - cwiid.BTN_2 - cwiid.BTN_1 == 0):  
			print 'Buttons 1+2 pressed together'
			scroll = ~(wii.state['acc'][1]-125)
			if (scroll > 0): 		
				send("SCROLL-HIGH") #scroll upwards
			else: 
				send("SCROLL-LOW") #scroll downwards
		
		#right-click		
		elif (buttons & cwiid.BTN_B):
			print 'Button B pressed'
			send("R1")
		#left-click
		elif (buttons & cwiid.BTN_A):
			print 'Button A pressed'
			send("L1")

		#2 control x-axis			
		elif (buttons & cwiid.BTN_2):			
			x += ~(wii.state['acc'][1]-125)
			print(x)		
			coords = str(x)+","+str(y)
			send(coords)
		#1 control y-axis
		elif (buttons & cwiid.BTN_1):
			y += (wii.state['acc'][1]-125)
			print(y)
			coords = str(x)+","+str(y)
			send(coords)
			
		## Check if other buttons are pressed by doing a bitwise AND of the buttons number and the predefined constant for that button
		
		#Scroll-LEFT
		elif (buttons & cwiid.BTN_LEFT):
			print 'Left pressed'
			send("LEFT")
		#Scroll-RIGHT
		elif(buttons & cwiid.BTN_RIGHT):
			print 'Right pressed'
			send("RIGHT")
		#Scroll-UP
		elif (buttons & cwiid.BTN_UP):
			print 'Up pressed'
			send("UP")
		#Scroll-DOWN
		elif (buttons & cwiid.BTN_DOWN):
			print 'Down pressed'
			send("DOWN")

		#MINUS
		elif (buttons & cwiid.BTN_MINUS):
			print 'Minus Button pressed'
			send("BACK")
		#HOME
		elif (buttons & cwiid.BTN_HOME):
			print 'Home Button pressed'
			send("HOME")
		#PLUS
		elif (buttons & cwiid.BTN_PLUS):
			print 'Plus Button pressed'
			send("ENTER")

		#WiiMote is still			
		else:
			print("...")
		time.sleep(button_delay)

finally:
	print >>sys.stderr, 'closing socket'
	sock.close()
