/**
 *
 * @author bcmoney
 */
import com.sun.glass.events.KeyEvent;
import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.HeadlessException;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.PointerInfo;
import java.awt.Toolkit;
import java.net.*; 
import java.io.*; 

public class TcpServer extends Thread { 
    
    protected static boolean serverContinue = true;
    protected Socket clientSocket;
    final static int PORT = 10008; //random unlikely to be used port
    

    /* constructor */
    private TcpServer (Socket clientSoc) {
        clientSocket = clientSoc;
        start();
    }
    
   /**
    * getScreenSize
    *   Checks if the user has a multi-screen monitor setup or a single-monitor,
    *   then attempts to get the Screen size (as pixel resolution).
    * @return String   Screen dimensions as WxH 
    */
    private String getScreenSize() {
        String w = "";
        String h = "";
        try {
            if (GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice() != null) {
                GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
                int width = gd.getDisplayMode().getWidth();
                w = ""+width;
                int height = gd.getDisplayMode().getHeight();
                h = ""+height;
            } else {
                Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
                double width = screenSize.getWidth();
                w = ""+width;
                double height = screenSize.getHeight();
                h = ""+height;
            }            
        } catch(HeadlessException ex) {
            ex.printStackTrace();
        }
        return w+"x"+h;
    }

   /**
    * getCursorLocation
    *   Checks for the current position of the mouse (cursor/pointer) based on
    *   initial X,Y positioning.
    * @return String   Cursor position as X,Y 
    */
    private String getCursorLocation() {
        PointerInfo a = MouseInfo.getPointerInfo();
        Point startingPoint = a.getLocation();
        int startingX = (int) startingPoint.getX();
        int startingY = (int) startingPoint.getY();
        return startingX+","+startingY;
    }
    
    /**
     * runs the server when called 
     */
    public void run() {
        System.out.println ("New Communication Thread Started");
        try { 
            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true); 
            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            //communicate Screen-size & starting X/Y points once to synch up, as part of handshake
            out.println(getScreenSize()+"|"+getCursorLocation());

            MouseMove mm = new MouseMove();
            MouseClick mc = new MouseClick();
            try {            
                String inputLine; 
                while ((inputLine = in.readLine()) != null) {
                    System.out.println("SERVER - Sending: " + inputLine); 
                    out.println(inputLine);

                    if (inputLine.equals("BYE")) {
                        break; //disconnect but leave server running
                    } else if (inputLine.equals("EXIT")) {
                        serverContinue = false; //close the server completely
                    } else if (inputLine.equals("BACK")) {
                        mc.keyboardPress(KeyEvent.VK_BACKSPACE);//hit BACKSPACE on keyboard
                    } else if (inputLine.equals("HOME")) {                        
                        mc.keyboardPress(KeyEvent.VK_HOME);//hit Windows-->StartMenu on keyboard
                    } else if (inputLine.equals("ENTER")) {
                        mc.keyboardPress(KeyEvent.VK_ENTER);//hit ENTER on keyboard
                    } else if (inputLine.contains("LEFT")) {
                        mc.keyboardPress(KeyEvent.VK_LEFT);//hit <-- arrow on keyboard
                    } else if (inputLine.contains("RIGHT")) {
                        mc.keyboardPress(KeyEvent.VK_RIGHT);//hit --> arrow on keyboard
                    } else if (inputLine.contains("UP")) {
                        mc.keyboardPress(KeyEvent.VK_UP);//hit '^' arrow on keyboard
                    } else if (inputLine.contains("DOWN")) {
                        mc.keyboardPress(KeyEvent.VK_DOWN);//hit 'v' arrow on keyboard                                
                    } else {
                        //parse X,Y into MouseMove
                        if(inputLine.contains(",")) {
                            int x = Integer.parseInt(inputLine.trim().split(",")[0]);
                            int y = Integer.parseInt(inputLine.trim().split(",")[1]);                            
                            mm.setX(x);
                            mm.setY(y);
                            mm.moveMouse();
                        } else if(inputLine.contains("L")) {
    //                      boolean lbd = Boolean.parseBoolean(inputLine.trim().split("L")[1]); //Left Button Down                            
                            mc.click();
                        } else if(inputLine.contains("SCROLL")) {
    //                      boolean sbd = Boolean.parseBoolean(inputLine.trim().split("M")[1]); //Middle (Scroll) Button Down                            
                            int amt = 0;
                            String s = inputLine.trim().split("-")[1];
                            if (s.equals("HIGH")) {
                                amt = 40;
                            } else {
                                amt = -40;
                            }   
                            mc.scroll(amt);                            
                        } else if(inputLine.contains("R")) {
    //                      boolean rbd = Boolean.parseBoolean(inputLine.trim().split("R")[1]); //Right Button Down
                            mc.rightClick();
                        }
                        else {
                            out.println("Invalid input, please send 'X,Y' -or- Button presses");
                        }
                    }
                } 
            } catch(AWTException ex) {
                ex.printStackTrace();
            } catch(NumberFormatException nEx) {
                nEx.printStackTrace();                        
            }

            out.close(); 
            in.close(); 
            clientSocket.close();
            
        } catch (IOException e) { 
             System.err.println("Problem with Communication Server");
             System.exit(1); 
        } 
    }
    
    
    /* Command-line activation of server/listener */
    public static void main(String[] args) throws IOException { 
        ServerSocket serverSocket = null; 

        // Obtain the InetAddress of the computer on which this program is running
        InetAddress localaddr = InetAddress.getLocalHost();        
        
        try { 
            serverSocket = new ServerSocket(PORT); 
            System.out.println ("Connection Socket running on " + localaddr + ":" + PORT);
            try {
                while(serverContinue) {
                  System.out.println ("Waiting for Connection");
                  new TcpServer(serverSocket.accept()); 
                 }
             } catch (IOException e) { 
                System.err.println("Accept failed."); 
                System.exit(1); 
             }
        } catch (IOException e) { 
            System.err.println("Could not listen on port: ." + PORT); 
            System.exit(1);
        } finally {
            try {
              serverSocket.close(); 
            } catch (IOException e) { 
                System.err.println("Could not close port: ." + PORT); 
                System.exit(1); 
            } 
        }
    }
    
} 
