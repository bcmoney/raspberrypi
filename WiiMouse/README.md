-----------------------
Bluetooth dongle compatibility:
-----------------------
lsusb


-----------------------
Install Bluetooth & tools:
-----------------------
sudo apt-get install bluetooth blueman bluez lswm wmgui wminput autoconf build-essential libcwiid-dev python-cwiid xpdf


-----------------------
Confirm Bluetooth started:
-----------------------
sudo service bluetooth status


-----------------------
Start Bluetooth adaptor:
-----------------------
sudo /etc/init.d/bluetooth start


-----------------------
Make Bluetooth adapter discoverable:
-----------------------
sudo hciconfig hci0 piscan


-----------------------
Check Bluetooth dongle plugged in:
-----------------------
hcitool dev


-----------------------
Check device(s) available:
-----------------------
hcitool scan


-----------------------
Get your PI's IP Address:
-----------------------
ifconfig


-----------------------
Replace the WiiMouse listener with latest code:
-----------------------
sudo rm WiiMouse.py
sudo nano WiiMouse.py
-->Paste
-->CTRL+X
-->Y
-->ENTER
sudo chmod 755 WiiMouse.py


-----------------------
Run Python Wii listener:
-----------------------
./WiiMouse.py <HOST> <PORT>
-OR-
sudo python wiimotetest.py
-OR-
sudo python wii_remote_1.py

pi@raspberrypi:~/Documents/Python $