import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.InputEvent;

/**
 *
 * @author bcmoney
 */
public class MouseClick {

    
    public void MouseClick() {}
    
    public void click() throws AWTException {
        Robot bot = new Robot();
        bot.mousePress(InputEvent.BUTTON1_MASK);
        bot.mouseRelease(InputEvent.BUTTON1_MASK);
    }
    
    public void scroll(int amount) throws AWTException {
        Robot bot = new Robot();
        bot.mousePress(InputEvent.BUTTON2_MASK);
        bot.mouseWheel(amount);
        bot.mouseRelease(InputEvent.BUTTON2_MASK);
    }
    
    public void rightClick() throws AWTException {
        Robot bot = new Robot();
        bot.mousePress(InputEvent.BUTTON3_MASK);
        bot.mouseRelease(InputEvent.BUTTON3_MASK);
    }

    /* Simulate a key press */
    public void keyboardPress(int keycode) throws AWTException {
        Robot bot = new Robot();
        bot.keyPress(keycode);
    }
            
        
    
}
