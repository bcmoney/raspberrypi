This demo uses Node.JS to push data collected via Internet Of Things (IoT) sensors or devices attached to the RaspberryPI.
On the front-end it requires the Socket.IO client library along with DOM manipulation (either vanilla JS or a library like jQuery, Angular, React, etc).

-----------------------
Install Socket.io library
-----------------------
npm install socket.io


-----------------------
Start Mosquitto server:
-----------------------
sudo /etc/init.d/mosquitto start


-----------------------
Stop Mosquitto server (after finishing experiments or actual IoT sensor monitoring):
-----------------------
sudo /etc/init.d/mosquitto stop


-----------------------
Subscribe to a topic (to test/verify data coming through properly via MQTT protocol):
-----------------------
mosquitto_sub -h 127.0.0.1 -t pusher


-----------------------
Publish to a topic (to simulate data inputs, ie. changing temperature as monitored by a sensor or SmartThermostat that supports MQTT):
-----------------------
mosquitto_pub -t pusher -m "25C"


-----------------------
Install Apache web server:
-----------------------
sudo apt-get install apache2 -y


-----------------------
Drop files on server (commands may require "sudo"):
-----------------------
mkdir /var/www/html/IoT/
cd /var/www/html/IoT/
scp -r IoT <PI_USERNAME>@<PI_SSH_HOST>:<PI_SSH_PORT>.
~~Alternatively, just pull via GIT directly to the PI directory you created~~


-----------------------
See output in browser:
-----------------------
http://localhost/IoT/pusher.html


-------------------------
Get your PI's IP Address:
-------------------------
ifconfig


-------------------------
See output from browser of another device on you network:
-------------------------
http://192.168.2.14/IoT/pusher.html


Optional steps...
Setup Static IP address for your PI and/or a DNS routing solution such as DynDNS to make your PI accessible to external networks 
(so you can access on the go, from Mobile devices, work computer, laptops/deskops outside the home, etc). However, if you make it 
accessible externally you should take serious considerations to Security (safe passwords, non-default user/IP/Port configs, 
authentication, authorization, Firewalls, IPsec/monitoring, IP whitelists/blacklists, XSS/XSRF prevention, etc).