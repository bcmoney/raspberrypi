#!/user/bin/env node

var sys = require('sys');
var net = require('net');
var mqtt = require('mqtt');

var HOST = 'mqtt://192.168.2.14'; //host name and protocol, i.e. 127.0.0.1
var PORT = 5000; //port number not commonly in use, i.e: 1883
var TOPIC = 'pusher'; //topic name, something generic like 'pusher' to which all devices/sensors feed data, or, device/sensor-specific

var io  = require('socket.io').listen(PORT);
var client = mqtt.connect(HOST);


io.sockets.on('connection', function (socket) {
  socket.on('subscribe', function (data) {
    console.log('Subscribing to: ' + data.topic);
    socket.join(data.topic);
    client.subscribe(data.topic);
  });
  //when socket connection publishes message, forward it to MQTT broker
  socket.on('publish', function(data){
    console.log('Publishing to: ' + data.topic);
    client.publish(data.topic, data.payload);
  });
});


client.on('message', function(topic, payload, packet) {
  console.log(topic + '=' + payload);
  io.sockets.emit('mqtt',{'topic':String(topic),
                          'payload':String(payload)
  });
});