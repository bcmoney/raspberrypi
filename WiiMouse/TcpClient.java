import java.io.*;
import java.net.*;

/**
 *
 * @author bcmoney
 */
public class TcpClient {
    
    public TcpClient() { }

    public static void main(String[] args) throws IOException {

        String serverHostname = "";
        int serverPort = 0;
        if (args.length > 0 && args.length < 2) {
           serverHostname = args[0];
           serverPort = 10008;
        } else if (args.length >= 3) {
           serverHostname = args[0];
           serverPort = Integer.parseInt(args[1]);            
        } else { 
            serverHostname = "192.168.2.18"; //127.0.0.1 or localhost
            serverPort = 10008;
        }
        
        System.out.println("Attemping to connect to host " + serverHostname + " on port " + serverPort);

        Socket echoSocket = null;
        PrintWriter out = null;
        BufferedReader in = null;

        try {
            echoSocket = new Socket(serverHostname, serverPort);
            out = new PrintWriter(echoSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(echoSocket.getInputStream()));
        } catch (UnknownHostException e) {
            System.err.println("Don't know about host: " + serverHostname);
            System.exit(1);
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for the connection to: " + serverHostname);
            System.exit(1);
        }

        BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
        String userInput;

        System.out.println("Type Message (\"Exit.\" to quit)");
        while ((userInput = stdIn.readLine()) != null) {
            out.println(userInput);

            // end loop
            if (userInput.equals("Exit.")) {
                break;
            }

            System.out.println("echo: " + in.readLine());
        }

        out.close();
        in.close();
        stdIn.close();
        echoSocket.close();
    }
}
