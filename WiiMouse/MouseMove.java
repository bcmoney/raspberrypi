/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.awt.AWTException;
import java.awt.Robot;

/**
 *
 * @author mcham_000
 */
public class MouseMove {

    // These coordinates are the default starting screen coordinates
    int xCoord = 500;
    int yCoord = 500;
    
    public void MouseMove() {} 
    
    public int getX() {
        return xCoord;
    }
    public void setX(int x) {
        this.xCoord = x;
    }
    
    public int getY() {
        return yCoord;
    }
    public void setY(int y) {
        this.yCoord = y;
    }
    
    public String moveMouse() throws AWTException {
        Robot robot = new Robot();
        robot.mouseMove(getX(), getY());   
        return getX()+","+getY();
    }
    
}
